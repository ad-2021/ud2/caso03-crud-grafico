package controladorbd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import modelo.Cliente;

public class PersistenciaCliente {

	private final Statement stmt;
	private final ResultSet rs;

	public PersistenciaCliente() throws SQLException {		
		stmt = ConexionBD.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		rs = stmt.executeQuery("select * from empresa_ad.clientes");
	}

	public Cliente getActual() throws SQLException {
		return leer();
	}

	public Cliente getPrimero() throws SQLException {
		if (totalRegistros() > 0) {
			rs.first();
			return leer();
		}
		return null;
	}

	public Cliente getAnterior() throws SQLException {
		if (totalRegistros() > 0) {
			rs.previous();
			if (rs.isBeforeFirst()) {
				rs.first();
			}
			return leer();
		}
		return null;
	}

	public Cliente getSiguiente() throws SQLException {
		if (totalRegistros() > 0) {
			rs.next();
			if (rs.isAfterLast()) {
				rs.last();
			}
			return leer();
		}
		return null;
	}

	public Cliente getUltimo() throws SQLException {
		if (totalRegistros() > 0) {
			rs.last();
			return leer();
		}
		return null;
	}

	public Cliente ir(int r) throws SQLException {
		rs.absolute(r);
		return leer();
	}

	public boolean esPrimero() throws SQLException {
		return rs.isFirst();
	}

	public boolean esAntesPrimero() throws SQLException {
		return rs.isBeforeFirst();
	}

	public void irAntesPrimero() throws SQLException {
		rs.beforeFirst();
	}

	public boolean esUltimo() throws SQLException {
		return rs.isLast();
	}

	public void insertar(Cliente cli) throws SQLException {
		rs.moveToInsertRow();
		rs.updateString(2, cli.getNombre());
		rs.updateString(3, cli.getDireccion());
		rs.insertRow();
	}

	public void modificar(Cliente cli) throws SQLException {
		rs.updateString(2, cli.getNombre());
		rs.updateString(3, cli.getDireccion());
		rs.updateRow();
	}

	private Cliente leer() throws SQLException {
		return new Cliente(rs.getInt(1), rs.getString(2), rs.getString(3));
	}

	public void borrar() throws SQLException {
		rs.deleteRow();
	}
	
	public int registroActual() throws SQLException {
		return rs.getRow();
	}

	public int totalRegistros() throws SQLException {
		int bak = rs.getRow();
		rs.last();
		int num = rs.getRow();
		rs.absolute(bak);
		return num;
	}

	public void cerrar() throws SQLException {
		stmt.close();
		rs.close();
		ConexionBD.cerrar();
	}


}
