package vista;

import controladorbd.PersistenciaCliente;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import modelo.Cliente;

public class ControllerCliente implements Initializable {

    private final static int MODO_NAVEGACION = 0;
    private final static int MODO_NUEVO_REGISTRO = 1;
    private final static int MODO_EDITA_REGISTRO = 2;

    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnBorrarOAceptar;
    @FXML
    private Button btnEditarOCancelar;
    @FXML
    private TextField tfID;
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfDireccion;
    @FXML
    private Label lblInfo;

    private PersistenciaCliente clibd;
    private Cliente cli;
    private int modo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfID.setDisable(true);
        try {
            clibd = new PersistenciaCliente();
            cli = clibd.getPrimero();
            mostrarRegistro();
        } catch (SQLException ex) {
        	Util.mensajeExcepcion(ex, "Conectando con la base de datos...");
            Platform.exit();
        }

    }

    // ******************************************************************************
    // ACCIONES ASOCIADAS A BOTONES
    // ******************************************************************************
    @FXML
    private void accionPrimero() {
        try {
            cli = (Cliente) clibd.getPrimero();
            mostrarRegistro();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
    }

    @FXML
    private void accionAtras() {
        try {
            cli = (Cliente) clibd.getAnterior();
            mostrarRegistro();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
    }

    @FXML
    private void accionAdelante() {
        try {
            cli = (Cliente) clibd.getSiguiente();
            mostrarRegistro();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
    }

    @FXML
    private void accionUltimo() {
        try {
            cli = (Cliente) clibd.getUltimo();
            mostrarRegistro();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
    }

    @FXML
    private void accionNuevo() {
        modo = MODO_NUEVO_REGISTRO;
        cambiarModo();

    }

    @FXML
    private void accionEditarOCancelar() {
        if (modo == MODO_NAVEGACION) {          // accion editar
            try {
                if (clibd.totalRegistros() > 0) {
                    modo = MODO_EDITA_REGISTRO;
                    cambiarModo();
                }
            } catch (SQLException ex) {
            	Util.mensajeExcepcion(ex, "Editando registro...");
            }
        } else {                                // accion cancelar//            
            mostrarRegistro();
            modo = MODO_NAVEGACION;
            cambiarModo();
        }

    }

    @FXML
    private void accionBorrarOAceptar() {
        if (modo == MODO_NAVEGACION) {   // accion borrar   
            try {
                if (clibd.totalRegistros() > 0) {
                    String mensaje = "¿Estás seguro de borrar el registro [" + tfID.getText() + "]?";
                    Alert d = new Alert(Alert.AlertType.CONFIRMATION, mensaje, ButtonType.YES, ButtonType.NO);
                    d.setTitle("Borrado de registro");
                    d.showAndWait();
                    if (d.getResult() == ButtonType.YES) {
                        clibd.borrar();       
                        if (clibd.totalRegistros()==0) {
                        	cli = null;
                        }
                        else {
                        	if (clibd.esAntesPrimero()) {                        
                        		cli = clibd.getPrimero();
                        	}
                        	else {
                        		cli = clibd.getActual();
                        	}
                        }
                        mostrarRegistro();
                    }
                }
            } catch (SQLException ex) {
                Util.mensajeExcepcion(ex, "Borrando registro...");
            }
        } else {                            // accion aceptar
            try {
                Cliente c = new Cliente(tfNombre.getText(), tfDireccion.getText());
                if (modo == MODO_NUEVO_REGISTRO) {
                    clibd.insertar(c);
                    cli = (Cliente) clibd.getUltimo();
                } else {
                    clibd.modificar(c);
                    cli = (Cliente) clibd.getActual();
                }
                mostrarRegistro();
                modo = MODO_NAVEGACION;
                cambiarModo();
            } catch (SQLException ex) {
            	Util.mensajeExcepcion(ex, "Actualizando registro...");
            }

        }
    }

    private void mostrarRegistro() {
        try {
            lblInfo.setText("Registro " + clibd.registroActual() + " de " + clibd.totalRegistros());
        } catch (SQLException ex) {
            Util.mensajeExcepcion(ex, "Mostrando registro...");
        }
        if (cli != null) {
            tfID.setText(String.valueOf(cli.getId()));
            tfNombre.setText(cli.getNombre());
            tfDireccion.setText(cli.getDireccion());
        } else {
            tfID.setText(String.valueOf(0));
            tfNombre.setText("");
            tfDireccion.setText("");
        }
    }

    private void cambiarModo() {
        switch (modo) {
            case MODO_NAVEGACION:
                btnNuevo.setDisable(false);
                btnBorrarOAceptar.setText("Borrar");
                btnEditarOCancelar.setText("Editar");
                tfNombre.setEditable(false);
                tfDireccion.setEditable(false);
                break;
            case MODO_NUEVO_REGISTRO:
                tfID.setText("<autonum>");
                tfNombre.setText("");
                tfDireccion.setText("");
            case MODO_EDITA_REGISTRO:
                btnNuevo.setDisable(true);
                btnBorrarOAceptar.setText("Aceptar");
                btnEditarOCancelar.setText("Cancelar");
                tfNombre.setEditable(true);
                tfDireccion.setEditable(true);
                tfNombre.requestFocus();

        }

    }

   
}
